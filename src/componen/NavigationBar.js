import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import React, { useState, useEffect } from "react";
import Placeholder from "react-bootstrap/Placeholder";
import { FaOpencart} from "react-icons/fa";
import { FaUserAlt } from "react-icons/fa";
import { Modal, Form, InputGroup } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Swal from "sweetalert2";
import axios from "axios";

// import { useHistory } from "react-router-dom";
function NavigationBar() {
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState(Number);
  const [img, setImg] = useState("");
  const [show, setShow] = useState(false);
  const [profil, setProfil] = useState(false);
  const [user, setUser] = useState([]);
  const [file, setFile] = useState();
 
  const handleShow1 = () => setProfil(true);
  const handleClose1 = () => setProfil(false);
  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const addList = async (e) => {
    e.preventDefault();
    e.persist();

      const formData = new FormData();
      formData.append("file", img);
      formData.append("nama", nama);
      formData.append("harga", harga);
      formData.append("deskripsi", deskripsi);
      // Send the file and description to the server
    try {
      await axios.post("http://localhost:5000/Produk", formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "multipart/form-data",
        },
      });
      setShow(false);
      Swal.fire("Good job!", "You clicked the button!", "success");
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };
  const Profile = async () => {
    await axios
      .get("http://localhost:5000/user/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data)
      })
      .catch((error) => {
        // alert("Terjadi keasalahan" + error);
      });
  };
  console.log(user);
  useEffect(() => {
    Profile();
  }, []);
  // const addMakanan = async (e) => {
  //   e.preventDefault();

  //   const data = {
  //     nama: nama,
  //     deskripsi: deskripsi,
  //     harga: harga,
  //     img: img,
  //     terjual: terjual,
  //   };
  //   await axios

  //     .post("http://localhost:8000/Makanans", data)

  //     .then(() => {
  //       Swal.fire({
  //         icon: "success",
  //         title: "Berhasil Di Tambahkan",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //       setTimeout(() => {
  //         window.location.reload();
  //       }, 1000);
  //     })
  //     .catch((eror) => {
  //       Swal.fire({
  //         icon: "error",
  //         title: "Your work has been saved",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //     });
  // };

  const logout = () => {
    localStorage.clear();
    // history.push("/login");
    window.location.reload();
  };
  return (
    <div>
      <Navbar className="bg-gradient-to-r from-orange-600 via-orange-500 to-orange-400 drop-shadow-lg" expand="lg fixed">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-10 h-10 "
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M13.5 21v-7.5a.75.75 0 01.75-.75h3a.75.75 0 01.75.75V21m-4.5 0H2.36m11.14 0H18m0 0h3.64m-1.39 0V9.349m-16.5 11.65V9.35m0 0a3.001 3.001 0 003.75-.615A2.993 2.993 0 009.75 9.75c.896 0 1.7-.393 2.25-1.016a2.993 2.993 0 002.25 1.016c.896 0 1.7-.393 2.25-1.016a3.001 3.001 0 003.75.614m-16.5 0a3.004 3.004 0 01-.621-4.72L4.318 3.44A1.5 1.5 0 015.378 3h13.243a1.5 1.5 0 011.06.44l1.19 1.189a3 3 0 01-.621 4.72m-13.5 8.65h3.75a.75.75 0 00.75-.75V13.5a.75.75 0 00-.75-.75H6.75a.75.75 0 00-.75.75v3.75c0 .415.336.75.75.75z"
          />
        </svg>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/" className="text-white">
              <h4> Angkringan</h4>
            </Nav.Link>
            <button>
              <Nav.Link href="/" className="text-white">
                Beranda
              </Nav.Link>
            </button>
            {localStorage.getItem("role") !== "ADMIN" ? (
              <></>
            ) : (
              <>
                {" "}
                <button className="btn text-white" onClick={handleShow}>
                  Tambah Barang
                </button>
              </>
            )}
            <button>
              <Nav.Link href="/makanan" className="text-white">
                Menu
              </Nav.Link>
            </button>
          </Nav>
        </Navbar.Collapse>
        <button className="">
          {localStorage.getItem("role") !== null ? (
            <>
              <a href="/" className="btn text-white" onClick={logout}>
                Logout
              </a>
            </>
          ) : (
            <>
              <a className="btn text-white" href="/login">
                Login
              </a>
            </>
          )}
        </button>
        {localStorage.getItem("role") !== "USER" ? (
              <></>
            ) : (
              <>
                {" "}
                <a href="/cart">
          <FaOpencart
            style={{ marginLeft: 20, marginRight: 20, fontSize: 40 }}
          />
        </a>
              </>
            )}
     
    
      <FaUserAlt   style={{ marginLeft: 20, marginRight: 20, fontSize: 30  }} className="text-blue-500 hover:text-blue-400 hover:scale-105" onClick={handleShow1}/>
      </Navbar>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Modal title</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={addList}>
            <Form.Group className="mb-3" controlId="formBasictext">
              <Form.Label>Judul</Form.Label>
              <Form.Control
                type="text"
                placeholder="Masukan Nama Makanan"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                required
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicText">
              <Form.Label>Deskripsi</Form.Label>
              <Form.Control
                type="text"
                placeholder="Deskripsi"
                value={deskripsi}onChange={(e) => setDeskripsi(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicDate">
              <Form.Label>Harga</Form.Label>
              <Form.Control
               
                type="Number"
                placeholder=""
                value={harga}onChange={(e) => setHarga(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicText">
              <Form.Label>Gambar</Form.Label>
              <Form.Control
                onChange={(e) => setImg(e.target.files[0])}
                type="file"
                placeholder="url"
                required
              />
            </Form.Group>

            {/* jika di close maka akan keluar */}
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            {/* jika di save maka akan menyimpan */}
            <Button variant="primary" type="submit">
              Save Changes
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
     {/* profi */}
     
      <Modal show={profil} onHide={handleClose1}>
        <Modal.Header closeButton >
         
          <Modal.Title>Profil</Modal.Title>
        </Modal.Header>
      <div
  class="relative block overflow-hidden rounded-lg border border-gray-100 p-8"
>
  <span
    class="absolute inset-x-0 bottom-0 h-2 bg-gradient-to-r from-green-300 via-blue-500 to-purple-600"
  ></span>

  <div class="justify-between sm:flex">
    <div>
      <h3 class="text-xl font-bold text-gray-900">
      {user.nama}
      </h3>

      <p class="mt-1 text-xs font-medium text-gray-600">Gender</p>
    </div>

    <div class="ml-3 hidden flex-shrink-0 sm:block">
      <img
        alt="Paul Clapton"
        src={user.foto}
        class="h-16 w-16 rounded-lg object-cover shadow-sm"
      />
    </div>
  </div>

  <div class="mt-4 sm:pr-8">
    <p class="text-sm text-gray-500">
     Alamat :{user.alamat} 
    </p>
  </div>

  <div class="mt-6 flex">
    <div class="flex flex-col-reverse">
      <dt class="text-sm font-medium text-gray-600">{user.nomor}</dt>
      <dd class="text-xs text-gray-500">Nomor</dd>
    </div>

    {/* <div class="ml-3 flex flex-col-reverse sm:ml-6">
      <dt class="text-sm font-medium text-gray-600">Reading time</dt>
      <dd class="text-xs text-gray-500">3 minute</dd>
    </div> */}
  </div>
  </div>


        <Modal.Footer>
        <a href="/profil">
        <Button variant="primary" >
            Detail
          </Button>
          </a>
        
          
          
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default NavigationBar;
