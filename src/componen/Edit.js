import React, { useState, useEffect } from "react";
import { Form, InputGroup } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";
import { Button } from "react-bootstrap";
// import  "../style/pages.css"
// metho edit
function Edit() {
  const param = useParams();
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");
  const [img, setImg] = useState("");
  const [terjual, setTerjual] = useState("");
  // ini untuk menampilkan yang mau di edit
  const history = useHistory();
  useEffect(() => {
    axios
      .get("http://localhost:5000/Produk/" + param.id)
      .then((response) => {
        const newList = response.data.data;
        setNama(newList.nama);
        setDeskripsi(newList.deskripsi);
        setHarga(newList.harga);
        setImg(newList.img);
      })
      .catch((eror) => {
        alert("terjadi kesalahan sir!" + eror);
      });
  }, []);
  const updateList = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", img)
    formData.append("nama", nama)
    formData.append("deskripsi", deskripsi)
    formData.append("harga", harga)

    Swal.fire({
      title: "Apakah ini di ganti?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Ya",
      denyButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:5000/Produk/" + param.id, formData,
            //headers berikut berfunsi untuk method yang berada di akses admin
            {
              headers: {
                Authorization: `Bearer${localStorage.getItem("token")}`,
                "Content-Type" : "multipart/form-data"
              },
            }
          )
          .then(() => {
            history.push("/makanan");
            Swal.fire("Berhasil!", "Data kamu di update", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  // ini untuk mengeditnya
//   const submitActionHandler = async (event) =>{
  //     event.preventDefault();
  //     await axios.put("http://localhost:8000/Makanans/" +param.id,{
  //         nama:nama,
  //         deskripsi:deskripsi,
  //         harga:harga,
  //         img:img,
  //         terjual:terjual,
  //     })
  //     .then(() => {
  //         Swal.fire({
  //             title: 'Do you want to save the changes?',
  //             showDenyButton: true,
  //             showCancelButton: true,
  //             confirmButtonText: 'Save',
  //             denyButtonText: 'Dont save',
  //           }).then((result) => {
  //             /* Read more about isConfirmed, isDenied below */
  //             setTimeout(() => {
  //                 history.push("/makanan");
  //                 window.location.reload();

  //             }, 1500);
  //             if (result.isConfirmed) {
  //               Swal.fire('Saved!', '', 'success')
  //             } else if (result.isDenied) {
  //               Swal.fire('Anda Tidak Merubah Data Apapun', '', 'info')
  //             }
  //           })

  //     })
  //     .catch((error) => {
  //         alert("Terjadi Kesalahan: " + error);
  //     });
  //   };

  return (
    <div className="bg-gradient-to-r from-orange-400 via-orange-500 to-orange-800 w-50 rounded-lg mx-auto py-1 mt-4">
     
    <div className="edit mx-5 ">
      <div className="container my-5">
        <Form onSubmit={updateList}>
          <div className="name mb-3">
            <Form.Label>
              <strong>name</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Judul"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </InputGroup>
          </div>
          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>deskripsi</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Deskripsi"
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
              />
            </InputGroup>
          </div>
          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Harga</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="text"
                placeholder="Harga"
                value={harga}
                onChange={(e) => setHarga(e.target.value)}
              />
            </InputGroup>
          </div>
      
          <div className="birth-date mb-3">
            <Form.Label>
              <strong>Img</strong>
            </Form.Label>
            <div className="d-flex gap-3">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="file"
                  placeholder="img"
                  onChange={(e) => setImg(e.target.files[0])}
                />
              </InputGroup>
            </div>
          </div>
          <div className="mt-2 mb-2 ">
            <Button variant="primary" className="buton btn" type="submit ">
              Save
            </Button>
          </div>
        </Form>
      </div>
    </div>
    </div>
  );
}

export default Edit;
