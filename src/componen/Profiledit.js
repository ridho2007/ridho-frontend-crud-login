import React, { useState, useEffect } from "react";
import { InputGroup, Form,Button } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import Swal from 'sweetalert2'

// method untuk membaca sistem dan bisa di ubah melalui input
export default function Edit() {
  const param = useParams();
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nomor, setNomor] = useState("");
  // const [kadarluwasa, setKadarluwasa] = useState("");
  const [foto, setFoto] = useState("");

  const history = useHistory();

  const updateList = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", foto)
    formData.append("nama", nama)
    formData.append("alamat", alamat)
    formData.append("nomor", nomor)

    Swal.fire({
      title: 'Yakin Untuk Mengedit',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yaa!'
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put("http://localhost:5000/user/" + param.id, formData, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
              "Content-Type": "multipart/form-data"
            },
          })
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      }
    })
      .then(() => {
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success'
        )
        history.push("/profil");
        window.location.reload()
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };
  useEffect(() => {
    axios
      .get("http://localhost:5000/user/" + param.id)
      .then((response) => {
        const newList = response.data.data;
        setNama(newList.nama);
        setAlamat(newList.alamat);
        setNomor(newList.nomor);
        setFoto(newList.foto);
      });
  }, []);



  return (
    <div >
    <div className="edit  bg-gradient-to-r from-orange-400 via-orange-500 to-orange-800 mx-[30%] p-[3%] my-[2%]">
      <Form onSubmit={updateList}>
        <div className="container my-5">
          <div className="name mb-3">
            <Form.Label>
              <strong className="">Nama</strong>
            </Form.Label>
            <InputGroup className="d-flex-gap-3">
              <Form.Control
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </InputGroup>
          </div>


          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong className="">Alamat</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">

          </div>

          <div className="place-of-birth mb-3">

          </div>

          <div className="place-date mb-3">
            <Form.Label>
              <strong className="">No.Telepon</strong>
            </Form.Label>
            <div className="d-flex gap-3">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  value={nomor}
                  onChange={(e) => setNomor(e.target.value)}
                />
              </InputGroup>
            </div>
          </div>

          <div className="place-date mb-3">
            <Form.Label>
              <strong className="">Image</strong>
            </Form.Label>
            <div className="d-flex gap-3">
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="file"
                  placeholder="Input Image"
                  onChange={(e) => setFoto(e.target.files[0])}
                  required
                />
              </InputGroup>
            </div>
          </div>
          <div className="d-flex justify-content-end align-center mt-2">
            {/* type submit untuk sistem yang telah membaca method bahwa berupa edit */}
            <Button className="buton btn " type="submit">
              Save
            </Button>
          </div>
        </div>
      </Form>
    </div>
    </div>
  );
}
