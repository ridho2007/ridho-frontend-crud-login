import React, { useEffect, useState } from "react";
import axios from "axios";
import Button from "react-bootstrap/Button";
import Swal from "sweetalert2";
import NavigationBar from "../componen/NavigationBar";
import Foter from "../componen/Foter";
import Cart from "./Cart";
import { InputGroup } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { data } from "autoprefixer";
function Makanan() {
  const [Makanan, setMakanan] = useState([]);
  const [nama, setNama] = useState("");
  const [harga, setHarga] = useState("");
  const [img, setImg] = useState("");
  const [list, setList] = useState([]);
  const [search, setSearch] = useState("");
  const [totalPage, setTotalPage] = useState(0);
  // const getAll = () => {
  //   axios
  //     .get("http://localhost:8000/Makanans")
  //     .then((res) => {
  // setMakanan(res.data);
  //     })
  //     .catch(() => {
  //       Swal.fire({
  //         icon: "error",
  //         title: "memunculkan data",
  //         showConfirmButton: false,
  //         timer: 1500,
  //       });
  //     });
  // };
  const deleteMakanan = async (id) => {
    await Swal.fire({
      title: "Kamu Yakin Ingin Menghapus",
      text: "Jika Di hapus maka Tidak Akan Kembali",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:5000/Produk/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        // setTimeout(() => {
        //   window.location.reload();
        // }, 2000);
      }
    });

    getAll(0);
  };
  useEffect(() => {
    getAll(0);
  }, []);

  const beli = (makanan) => {
    axios.post("http://localhost:5000/cart", {
      produkId: makanan.id,
      qty: makanan.qty,
      usersId: localStorage.getItem("userId"),
    });

    Swal.fire("Berhasil!", "Di masukan Ke keranjang", "success");
  };

  const getAll = async (idx) => {
    //liblary open source yang digunakan untuk request data melalui http.
    await axios
      .get("http://localhost:5000/Produk/all?page=" + idx + "&search=" + search)
      .then((res) => {
        setList(
          res.data.data.content.map((Makanan) => ({
            ...Makanan,
            qty: 1,
          }))
        );
        setTotalPage(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  const changePage = async (e) => {
    getAll(e);
  };

  const plusQuantityProduk = (idx) => {
    const listProduk = [...list];
    if (listProduk[idx].qty >= 45) {
      return;
    }
    listProduk[idx].qty++;

    setList(listProduk);
  };
  const minusQuantityProduk = (idx) => {
    const listProduk = [...list];
    if (listProduk[idx].qty <= 1) return;
    listProduk[idx].qty--;

    setList(listProduk);
  };
  return (
    <div className="bgimg">
      {" "}
      <NavigationBar />
      
      <div className="container my-3 ">
        {localStorage.getItem("role") === "user" && (
          <center>
            <h1>Selamat Datang Silahkan Pilih Menu</h1>
          </center>
        )}
        {localStorage.getItem("role") === "ADMIN" && (
          <center>
            {" "}
            <h1>Selamat Datang ADMIN</h1>
          </center>
        )}
        <hr />
        <Form className="d-flex w-50 m-auto">
          <Form.Control
            type="search"
            placeholder="Search"
            className="me-2 "
            aria-label="Search"
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <button
            onClick={(e) => {
              e.preventDefault();
              getAll(0);
            }}
            type="submit"
           className="bg-green-600 text-white rounded-lg px-[5%] hover:bg-green-500 hover:scale-105"
          >
            Search
          </button>
        </Form>
        <br />
        {list.length !== 0 ? (
          <>
            <div>
              <div className="grid grid-cols-4 gap-3 " >
                {list.map((Makanan, idx) => {
                  return (
                    <article class=" rounded-lg  w-[100%]  drop-shadow-xl shadow-lg">
                      <img
                        alt="Office"
                        src={Makanan.img}
                        class="h-56 w-full object-cover"
                      />

                      <div class=" sm:p-6">
                        <h3 class="text-lg font-medium text-gray-900">
                          {Makanan.nama}
                        </h3>
                        <p>{Makanan.deskripsi}</p>
                        <div className="flex gap-20">
                          <p class="mt-2 text-sm leading-relaxed text-gray-500 line-clamp-3">
                           Rp. {Makanan.harga}
                          </p>
                        </div>
                        <div>
                          <label for="Quantity" class="sr-only">
                            {" "}
                            Quantity{" "}
                          </label>

                          <div class="flex items-center border border-gray-200 rounded">
                            <button
                              type="button"
                              className={`text-lg w-20 h-10 leading-10  transition hover:opacity-75 ${
                                Makanan.qty <= 1 ? "grey" : "grean"
                              }`}
                              onClick={() => minusQuantityProduk(idx)}
                            >
                              &minus;
                            </button>
                            <input
                              type="number"
                              id="Quantity"
                              value={Makanan.qty}
                              class="h-10 w-16 border-transparent text-center [-moz-appearance:_textfield] sm:text-sm [&::-webkit-outer-spin-button]:m-0 [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-inner-spin-button]:appearance-none"
                            />

                            <button
                              type="button"
                              className={`text-lg w-20 h-10 leading-10  transition hover:opacity-75 ${
                                Makanan.qty >= 45 ? "grey" : "grean"
                              }`}
                              onClick={() => plusQuantityProduk(idx)}
                            >
                              +
                            </button>
                          </div>
                        </div>
                      </div>
                      <div className="flex">
                        {localStorage.getItem("role") === "user" ? (
                          <>
                            <a href="/login">
                              <button
                                onClick={() => beli(Makanan)}
                                className="ml-2 bg-amber-700 text-white rounded-lg p-2 hover:bg-amber-600 mb-3"
                              >
                                Login Untuk Memesan
                              </button>
                            </a>
                          </>
                        ) : (
                          <button
                            onClick={() => beli(Makanan)}
                            className="ml-2 bg-green-700 text-white rounded-lg px-4 py-2 hover:bg-green-600 hover:scale-105 mb-3 ring-2 ring-green-400"
                          >
                            Pesan
                          </button>
                        )}

                        {localStorage.getItem("role") === "ADMIN" && (
                          <button
                            
                            className="bg-red-600 ml-2 text-white rounded-lg px-3 py-2 mb-3 hover:bg-red-500 hover:scale-105 ring-2 ring-red-300"
                            onClick={() => deleteMakanan(Makanan.id)}
                          >
                            Hapus
                          </button>
                        )}
                        {/* tombol ini akan mengarah ke page lain */}
                        {localStorage.getItem("role") === "ADMIN" && (
                          <a href={"/edit/" + Makanan.id}>
                            <button  className="ml-2 bg-blue-600 text-white rounded-lg px-3 py-2 mb-3 hover:bg-blue-500 hover:scale-105 ring-2 ring-blue-200 ">
                              Edit
                            </button>
                          </a>
                        )}
                      </div>
                    </article>
                  );
                })}
              </div>
            </div>
            <ol class="flex justify-center gap-1 text-xs font-medium">
              <li>
                <a
                  href="#"
                  class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
                >
                  <span class="sr-only">Prev Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </a>
              </li>
              {createElements(totalPage, changePage)}
              <li>
                <a
                  href="#"
                  class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
                >
                  <span class="sr-only">Next Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </a>
              </li>
            </ol>
          </>
        ) : (
          <>
            <br />
            <div>
              {" "}
              <center>
                <h1>Yang Anda Cari Tidak Di Temukan</h1>
              </center>
            </div>
            <br />
          </>
        )}
      </div>
      <Foter />
    </div>
  );
}
function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer  block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}
export default Makanan;
