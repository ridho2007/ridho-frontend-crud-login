import React, { useState, useEffect } from "react";

import axios from "axios";
import NavigationBar from "../componen/NavigationBar";
import { FaUserAlt } from "react-icons/fa";
import { Modal, Form, InputGroup } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Swal from "sweetalert2";
import { useHistory, useParams } from "react-router-dom";
function Profil() {
  const param = useParams();
  const [foto, setFoto] = useState("");
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nomor, setNomor] = useState("");
  const history = useHistory();
  const [user, setUser] = useState([]);
  const [profil1, setProfil1] = useState(false);
  const Profile = async () => {
    await axios
      .get("http://localhost:5000/user/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data);
        setNama(res.data.data);
        setAlamat(res.data.data);
        setNomor(res.data.data);
        setFoto(res.data.data);
      })
      .catch((error) => {
        // alert("Terjadi keasalahan" + error);
      });
  };
  console.log(user);
  useEffect(() => {
    Profile();
  }, []);

 



  const updateList = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("file", foto)
    formData.append("nama", nama)
    formData.append("nomor", nomor)
    formData.append("alamat", alamat)

    Swal.fire({
      title: "Apakah ini di ganti?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Ya",
      denyButtonText: "Tidak",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            "http://localhost:5000/user/" + param.id,formData,
        
            //headers berikut berfunsi untuk method yang berada di akses admin
            {
              headers: {
                Authorization: `Bearer${localStorage.getItem("token")}`,
                "Content-Type" : "multipart/form-data"
             
              },
            }
          )
          .then(() => {
            history.push("/makanan");
            Swal.fire("Berhasil!", "Data kamu di update", "success");
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  };
  return (
    <div className="bgimg pb-72">
      <div>
      <NavigationBar />
      <div className="mx-[15%] mt-[5%]  drop-shadow-xl shadow-lg">
        {" "}
       
        <article class="rounded-xl border border-gray-700 bg-gradient-to-r from-orange-700  via-orange-600 to-orange-500 p-4  drop-shadow-xl shadow-lg">
          <div class="flex items-center">
            <img
              alt="Developer"
              src={user.foto}
              class="h-16 w-16 rounded-full object-cover"
            />

            <div class="ml-3">
              <h3 class="text-lg font-medium text-white">{user.nama}</h3>

              <div class="flow-root">
                <ul class="-m-1 flex flex-wrap">
                  <li class="p-1 leading-none">
                    <div class="text-xs font-medium text-gray-300">
                      {" "}
                      Twitter{" "}
                    </div>
                  </li>

                  <li class="p-1 leading-none">
                    <div class="text-xs font-medium text-gray-300">
                      {" "}
                      GitHub{" "}
                    </div>
                  </li>

                  <li class="p-1 leading-none">
                    <div class="text-xs font-medium text-gray-300">Website</div>
                  </li>
                </ul>
              </div>
            </div>
           
          </div>

          <ul class="mt-4 space-y-2">
            <li>
              <div class="block h-full rounded-lg border border-gray-700 p-4 hover:border-pink-600">
                <strong class="font-medium text-white">Nomor:</strong>

                <p class="mt-1 text-xs font-medium text-gray-300">
                 {user.nomor}
                </p>
              </div>
            </li>

            <li>
              <div class="block h-full rounded-lg border border-gray-700 p-4 hover:border-pink-600">
                <strong class="font-medium text-white">Alamat:</strong>
                <div class="text-left">
      
      </div>
                <p class="mt-1 text-xs font-medium text-gray-300">
                  {user.alamat}
                </p>
              </div>
            </li>
          </ul>
          <div className="text-center ">
          <a  href={`/profiledit/${user.id}`}><Button>Edit</Button></a>
          </div>
          
        </article>
      
      </div>
     
      </div>
    </div>
  );
}

export default Profil;
