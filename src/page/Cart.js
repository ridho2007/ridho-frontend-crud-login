import axios from "axios";
import React, { useState, useEffect } from "react";
import NavigationBar from "../componen/NavigationBar";
import Swal from "sweetalert2";
import "../css/Cart.css";
import { Button } from "react-bootstrap";
const Cart = ({ Makanan, setMakanan, handleChange }) => {
  const [img, setImg] = useState(0);
  const [nama, setNama] = useState(0);
  const [harga, setHarga] = useState(0);
  const [cart, setCart] = useState([]);
const [usersId, setUsersId] = useState([])
const [list, setList] = useState([])
const [totalPage, setTotalPage] = useState(0)
  const getAll = (idx) => {
    axios
      .get(`http://localhost:5000/cart?page=${idx}&usersId=${localStorage.getItem("userId")}`)
      .then((res) => {
    setCart(res.data.data.content);
    setTotalPage(res.data.data.totalPages)
      })
      .catch(() => {
        Swal.fire({
          icon: "error",
          title: "memunculkan data",
          showConfirmButton: false,
          timer: 1500,
        });
      });
  };
  const changePage = async (e) => {
    getAll(e); 

  };
  const deleteCart = async (id) => {
    await Swal.fire({
      title: "Apakah Kamu yakin?",
      text: "Ingin Menghapus Pesanan Ini",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Hapus!",
    }).then(async(result) => {
      if (result.isConfirmed) {
      await axios.delete("http://localhost:5000/cart/" + id);
        Swal.fire("Deleted!", "Your file has been deleted.", "success");
        getAll(0);
      }
    });

    
  };
  const checkout = async () => {
    await Swal.fire({
      title: "Yakin Ingin Membeli?",
      text: "Jika Ya Maka Pesanan Akan Di Antar!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya!",
    }).then(async(result) => {
      if (result.isConfirmed) {
      await axios.delete("http://localhost:5000/cart/{tuku}?usersId=" + localStorage.getItem("userId"));
        Swal.fire("Di pesan!", "Sedang Di Antar", "success");
        getAll(0);
      }
    })};
  useEffect(() => {
    getAll(0);
  }, []);
  console.log(Makanan);

  const konversionRupiah = (angka) => {
    return new Intl.NumberFormat("id-ID",{
      style: "currency",
      currency:"IDR",
    }).format(angka);

  }
  const TotalHarga = cart.reduce(function (result, item) {
    return result + item.totalharga;
  }, 0);

  return (
    <div className="bgimg pb-[100%]">
      <NavigationBar />
      <div className=" w-[90%]">
        <table 
          className="table table-bordered mx-[10%]  my-5  "
          
        >
          <thead>
            <tr>
              <th className="header">No</th>
              <th className="header">Gambar</th>
              <th className="header">Nama Barang</th>
              <th className="header">Jumlah</th>
              <th className="header">Harga Permenu</th>
              <th className="header">Total Harga</th>
              <th className="header">Action</th>
            </tr>
          </thead>
          <br />
          <tbody>
            {cart.map((data, index) => {
              return (
                <tr key={data.id}>
                  <td>{index + 1}</td>
                  <td className="w-40">
                    <img src={data.produkId.img} alt="" />{" "}
                  </td>
                  <td>{data.produkId.nama}</td>
                  <td>{data.qty}</td>
                  <td>{konversionRupiah(data.produkId.harga)}</td>
                  <td>{konversionRupiah(data.totalharga)}</td>
                  <td>
                    <Button
                      variant="danger"
                      className="ml-2 text-white rounded-lg p-2 mb-3 hover:scale-105"
                      onClick={() => deleteCart(data.id)}
                    >
                      Hapus
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="flex ">
          <div className="border-2 border-sky-500 w-60 m-4 text-center ">
            <p>
              Total Harga:Rp
              {konversionRupiah(TotalHarga)}
            </p>
          </div>
          <div className="pt-2">
            <button className="m-4 bg-amber-700 text-white rounded-lg p-2 hover:bg-amber-600 mb-3 hover:scale-105 "
              onClick={() => checkout()}>
              cekout
            </button>
          </div>
        </div>
        <ol class="flex justify-center gap-1 text-xs font-medium">
              <li>
                <a
                  href="#"
                  class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
                >
                  <span class="sr-only">Prev Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </a>
              </li>
              {createElements(totalPage, changePage)}
              <li>
                <a
                  href="#"
                  class="inline-flex h-8 w-8 items-center justify-center rounded border border-gray-100"
                >
                  <span class="sr-only">Next Page</span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="h-3 w-3"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </a>
              </li>
            </ol>
      </div>
    </div>
  );
};
function createElements(n, clickPage) {
  var elements = [];
  for (let i = 0; i < n; i++) {
    elements.push(
      <li>
        <span
          onClick={() => clickPage(i)}
          key={i}
          class="cursor-pointer  block h-8 w-8 rounded border border-gray-100 text-center leading-8"
        >
          {i + 1}
        </span>
      </li>
    );
  }
  return elements;
}
export default Cart;
