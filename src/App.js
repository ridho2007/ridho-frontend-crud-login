import { useState } from "react";
import { Card } from "react-bootstrap";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import Cart from "./page/Cart";
import Edit from "./componen/Edit";


import Home from "./page/Home";
import Login from "./componen/Login";
import PageAdmin from "./page/Profil";

import Register from "./componen/Register";
import Makanan from "./page/Makanan";
import Profiledit from "./componen/Profiledit";

function App() {
  
 

 


  return (
    <div className="aku">
      <div className="App">
        <BrowserRouter>
          <main>
            <Switch>
              
              <Route path="/register" component={Register} exact />
              <Route path="/" component={Home} exact />
              <Route path="/makanan" component={Makanan} exact />
              <Route path="/login" component={Login} exact />
              {/* ini untuk mengedit akan di ambil id nya */}
              <Route path="/edit/:id" component={Edit} exact />
              <Route path="/profiledit/:id" component={Profiledit} exact/>
              <Route path="/cart" component={Cart} exact />
              <Route path="/profil" component={PageAdmin} exact />
            </Switch>
          </main>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
